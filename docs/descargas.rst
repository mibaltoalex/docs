Descargas
================
 Centro de descargas donde conseguir las últimas aplicaciones, herramientas y
 mucho más ofrecido por nosotros y de nuestros colaboradores.

<https://dl.mibaltoalex.com>

* /android/[version]/[nombre].apk
* /desktop/[so]/[arch]/[version]/[nombre].[ext]
* /files/[hash]/[nombre].[ext]
