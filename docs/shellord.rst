ShellOrd
==========

ShellOrd es un framework que proporciona un conjunto de herramientas para la seguridad informática y ayuda en tests de penetración. 

Principales características: 

*  Compatible con Windows, Mac OS X, Linux.
*  Portable. No requiere instalación.
*  Admite carga y exportación de contenidos y/o módulos. 
*  Soporte de línea de comandos. 
*  Compatible con Metasploit. 
*  CyberChef integrado.
*  Lista de binarios de Unix y Windows que se pueden usar para eludir las restricciones de seguridad locales en sistemas mal configurados. 
*  Generación de snippets personalizados.

https://shellord.mibaltoalex.com
