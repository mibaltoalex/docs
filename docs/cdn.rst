CDN
================
 Nuestra Red de Distribución de Contenidos (CDN, Content Delivery Network)
 contiene librerías, imágenes, documentos, scripts
 y mucho más ofrecido por nosotros y nuestros colaboradores.

<https://cdn.mibaltoalex.com>

Estructura
-----------

* /libs/
* /img/
* /css/
* /js/
* /json/
* /fonts/
* /audio/
* /snippets/


DNS prefetch
--------
Para minimizar el impacto del tiempo de resolución DNS sobre el tiempo total de
carga de la página.

En la cabecera del código html de la página, debemos añadir el siguiente código
para aplicar esta técnica de precarga de DNS:

::

 <head>
  <!-- CDN MIBALTOALEX -->
  <link href='//cdn.mibaltoalex.com' rel='dns-prefetch'/>
  ..


Librerías
--------
jQuery, Bootstrap, FontAwesome, Popper.js

::

  <!-- jQuery -->
  <script src="https://cdn.mibaltoalex.com/libs/jquery/3.3.1/jquery-3.3.1.min.js" integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT" crossorigin="anonymous"></script>
  <!-- Bootstrap JS -->
  <script src="https://cdn.mibaltoalex.com/libs/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <!-- FontAwesome JS -->
  <script src="https://cdn.mibaltoalex.com/libs/fontawesome/5.4.1/js/all.min.js" integrity="sha384-L469/ELG4Bg9sDQbl0hvjMq8pOcqFgkSpwhwnslzvVVGpDjYJ6wJJyYjvG3u8XW7" crossorigin="anonymous"></script>
  <!-- Popper.js -->
  <script src="https://cdn.mibaltoalex.com/libs/popper.js/1.14.4/dist/umd/popper.min.js" integrity="sha384-GM0Y80ecpwKxF1D5XCrGanKusGDy9WW0O2sSM84neB4iFhvKp3fwnoIRnPsQcN1R" crossorigin="anonymous"></script>
